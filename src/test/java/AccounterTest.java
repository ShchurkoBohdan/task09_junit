import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.util.ArrayList;


public class AccounterTest {

    @org.junit.Test
    @Test
    public void arrayMinValuePositiveTest(){

        int min = Accounter.getMin(new int[]{1,2,3,4,15,6,0,8,9});
        System.out.println(min);
        Assertions.assertEquals(min, 0);

    }

    @org.junit.Test
    @Test
    public void arrayMaxValuePositiveTest(){

        int max = Accounter.getMax(new int[]{1,2,3,4,15,6,0,8,9});
        System.out.println(max);
        Assertions.assertEquals(max, 15);

    }

    @org.junit.Test
    @Test
    public void arrayMinValueNegativeTest() {

        int min = Accounter.getMin(new int[]{1, 2, 3, 4, 15, 6, 0, 8, 9});
        System.out.println(min);
        Assertions.assertFalse(min == 0, min +" equals " + 0);
    }

    @org.junit.Test
    @Test
    public void arrayMaxValueNegativeTest() {

        int max = Accounter.getMax(new int[]{1, 2, 3, 4, 15, 6, 0, 8, 9});
        System.out.println(max);
        Assertions.assertFalse(max != 15, max + " equals " + 15);
    }

    @org.junit.Test
    @Test
    public void isFileCreatedTest() {
        Accounter.createFile("test.txt");
        File directory = new File("/Users/shchurkobohdan/IdeaProjects/task09_JUNIT/");
        ArrayList<String> filenames = new ArrayList<String>();
        File[] files = directory.listFiles();
        for (File f : files){
            filenames.add(f.getName());
        }

        Assertions.assertTrue(filenames.contains("test.txt"));
    }

}
