import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

public class CalculatorTest {

    @org.junit.Test
    @Test
    public void addTest() {
        Randomizer randomizer = Mockito.mock(Randomizer.class);

        when(randomizer.getRandomNumber()).thenReturn(50);

        Assertions.assertTrue(Calculator.add(randomizer.getRandomNumber(), 2) == 52);
    }

    @org.junit.Test
    @Test
    public void multiplyTest() {
        Randomizer randomizer = Mockito.mock(Randomizer.class);

        when(randomizer.getRandomNumber()).thenReturn(35);

        Assertions.assertTrue(Calculator.multiply(randomizer.getRandomNumber(), 2) == 70);
    }

}
