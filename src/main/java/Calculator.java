public class Calculator {

    public static int add(int a, int b) {
        return a + b;
    }

    public static double divide(int a, int b) {
        return (double) a / b;
    }

    public static int multiply(int a, int b) {
        return a * b;
    }
}
