import java.io.File;
import java.io.IOException;

public class Accounter {
    public static int getMax(int [] arr) {


        int max = arr[0];

        // display max number
        for (int i = 1; i < arr  .length; i++){
            if ( arr[i] > max){
                max = arr[i];
            }
        }

        System.out.println("Max number is :" + max);

        return max;

    }
    public static int getMin(int [] arr) {
        int min = arr[0];
        // display min number
        for (int i = 1; i < arr  .length; i++){
            if ( arr[i] < min){
                min = arr[i];
            }
        }

        System.out.println("Min number is: " + min);
        return min;
    }

    public static void createFile(String filename){
        File file = new File("/Users/shchurkobohdan/IdeaProjects/task09_JUNIT/" + filename);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
